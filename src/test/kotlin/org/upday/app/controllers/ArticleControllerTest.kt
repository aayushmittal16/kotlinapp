package org.upday.app.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import java.time.LocalDateTime
import java.util.Optional
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.upday.app.controllers.ArticleController.Companion.ARTICLES_AUTHOR_URL
import org.upday.app.controllers.ArticleController.Companion.ARTICLES_KEYWORD_URL
import org.upday.app.controllers.ArticleController.Companion.ARTICLES_PERIOD_URL
import org.upday.app.controllers.ArticleController.Companion.ARTICLE_BASE_URL
import org.upday.app.controllers.ArticleController.Companion.ARTICLE_BASE_URL_WITH_ID
import org.upday.app.entities.Article
import org.upday.app.entities.Author
import org.upday.app.entities.Keyword
import org.upday.app.services.ArticleService

@WebMvcTest(ArticleController::class)
@RunWith(SpringRunner::class)
class ArticleControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var mapper: ObjectMapper

    @MockBean
    lateinit var articleService: ArticleService

    lateinit var article: Article
    lateinit var article2: Article

    @Before
    fun setup() {
        article =
            Article(
                "this is a title",
                "this is a description",
                "this is some content",
                LocalDateTime.now(),
                setOf(Author("author name")),
                setOf(Keyword("article")),
                1
            )

        article2 =
            Article(
                "title for article2",
                "description for article2",
                "content for article2",
                LocalDateTime.now(),
                setOf(Author("author2")),
                setOf(Keyword("keyword2")),
                2
            )
    }

    @Test
    fun `GET article by id`() {
        `when`(articleService.findById(1)).thenReturn(Optional.of(article))
        mockMvc.perform(get(ARTICLE_BASE_URL_WITH_ID, 1)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(content().string(mapper.writeValueAsString(article)))

        `when`(articleService.findById(2)).thenReturn(Optional.empty())
        mockMvc.perform(get(ARTICLE_BASE_URL_WITH_ID, 2)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound)
    }

    @Test
    fun `POST article`() {
        `when`(articleService.save(article)).then { }
        `when`(articleService.findById(article.id!!)).then { Optional.empty<Article>() }
        mockMvc.perform(
            post(ARTICLE_BASE_URL)
                .content(mapper.writeValueAsString(article))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isCreated)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(content().string(mapper.writeValueAsString(article)))
    }

    @Test
    fun `PATCH article`() {
        `when`(articleService.findById(article.id!!)).then { Optional.of(article) }
        `when`(articleService.save(article)).then { }
        mockMvc.perform(
            patch(ARTICLE_BASE_URL_WITH_ID, article.id)
                .content(mapper.writeValueAsString(article))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNoContent)

        `when`(articleService.findById(article.id!!)).then { Optional.empty<Article>() }
        mockMvc.perform(
            patch(ARTICLE_BASE_URL_WITH_ID, article.id)
                .content(mapper.writeValueAsString(article))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNotFound)
    }

    @Test
    fun `DELETE article`() {
        `when`(articleService.deleteById(article.id!!)).then { }
        `when`(articleService.findById(article.id!!)).then { Optional.of(article) }
        mockMvc.perform(
            delete(ARTICLE_BASE_URL_WITH_ID, article.id)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNoContent)

        `when`(articleService.findById(article.id!!)).then { Optional.empty<Article>() }
        mockMvc.perform(
            delete(ARTICLE_BASE_URL_WITH_ID, article.id)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNotFound)
    }

    @Test
    fun `GET all articles by author`() {
        var list = listOf(article)
        `when`(articleService.findAllByAuthor("author name")).thenReturn(list)
        mockMvc.perform(get(ARTICLES_AUTHOR_URL, "author name")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(content().string(mapper.writeValueAsString(list)))

        list = listOf()
        `when`(articleService.findAllByAuthor("unknown")).thenReturn(list)
        mockMvc.perform(get(ARTICLES_AUTHOR_URL, "unknown")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().string(mapper.writeValueAsString(list)))
    }

    @Test
    fun `GET all articles by keyword`() {
        var list = listOf(article2)
        `when`(articleService.findAllByKeyword("keyword2")).thenReturn(list)
        mockMvc.perform(get(ARTICLES_KEYWORD_URL, "keyword2")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(content().string(mapper.writeValueAsString(list)))

        list = listOf()
        `when`(articleService.findAllByKeyword("unknown")).thenReturn(list)
        mockMvc.perform(get(ARTICLES_KEYWORD_URL, "unknown")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().string(mapper.writeValueAsString(list)))
    }

    @Test
    fun `GET all articles by period`() {
        var list = listOf(article, article2)
        val from = LocalDateTime.MIN
        val to = LocalDateTime.MAX
        `when`(articleService.findAllByPeriod(from, to)).thenReturn(list)
        mockMvc.perform(
            get(ARTICLES_PERIOD_URL, from, to)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(content().string(mapper.writeValueAsString(list)))

        list = listOf()
        `when`(articleService.findAllByPeriod(from, to)).thenReturn(list)
        mockMvc.perform(
            get(
                ARTICLES_PERIOD_URL, to, to)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(content().string(mapper.writeValueAsString(list)))
    }
}
