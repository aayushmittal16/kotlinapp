package org.upday.app.repositories

import java.time.LocalDateTime
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.junit4.SpringRunner
import org.upday.app.entities.Article
import org.upday.app.entities.Author
import org.upday.app.entities.Keyword
import org.upday.app.persistence.ArticleRepository

@DataJpaTest
@RunWith(SpringRunner::class)
class ArticleRepositoryIntegrationTest {

    @Autowired
    lateinit var entityManager: TestEntityManager

    @Autowired
    lateinit var articleRepository: ArticleRepository

    lateinit var article: Article
    lateinit var article2: Article
    lateinit var author: Author
    lateinit var keyword: Keyword

    @Before
    fun setup() {
        article = Article(
            "My first app in Kotlin",
            "How I created my first app in Kotlin with Spring Boot",
            "This is my first app using Kotlin and Spring Boot. " +
                    "This is a test for an engineering position at upday. " + "" +
                    "It involves a tiny bit of coding and mostly magic using frameworks.",
            LocalDateTime.now(),
            mutableSetOf(Author("Aayush Mittal")),
            mutableSetOf(Keyword("Kotlin"), Keyword("Spring Boot"), Keyword("Programming"), Keyword("Engineering"))
        )

        author = Author("Thom Yorke")
        keyword = Keyword("Music")
        article2 = Article(
            "Radiohead on In Rainbows",
            "Thom Yorke and Ed O'Brien joined John Kennedy for an extensive " +
                    "talk through their brilliant new album.",
            "'In Rainbows' is one of their finest albums to date with the band perfectly " +
                    "balancing the mixture of electronics and rock anthems in a hugely enjoyable " +
                    "45 minutes.",
            LocalDateTime.now(),
            mutableSetOf(author, Author("Ed O'Brien"), Author("John Kennedy")),
            mutableSetOf(Keyword("Radiohead"), Keyword("In Rainbows"), keyword, Keyword("Best album of 2007"))
        )

        entityManager.persist(article)
        entityManager.flush()
        entityManager.persist(article2)
        entityManager.flush()
    }

    @Test
    fun `when find by ID then return Article`() {
        val firstArticle = articleRepository.findById(article.id!!).orElse(null)
        assertEquals(article, firstArticle)
    }

    @Test
    fun `when find all then return all Article objects`() {
        val articles = articleRepository.findAll()
        assertTrue(articles.contains(article))
        assertTrue(articles.contains(article2))
        assertEquals(2, articles.count())
    }

    @Test
    fun `given Article when saved then found `() {
        val newArticle = Article(
            "A short article",
            "How to write a useless article taught in 10 seconds",
            "Use a short title, a short description, and some jibberish content",
            LocalDateTime.now(),
            mutableSetOf(Author("Anonymous")),
            mutableSetOf(Keyword("Writing"), Keyword("Jibberish"))
        )
        articleRepository.save(newArticle)

        assertTrue(articleRepository.findById(newArticle.id!!).isPresent)
    }

    @Test
    fun `given Article when updated then changed`() {
        article.title = "This is a changed title"
        articleRepository.save(article)
        assertEquals(article.title, articleRepository.findById(article.id!!).get().title)
    }

    @Test
    fun `given Article when deleted then not found`() {
        articleRepository.delete(article)
        assertEquals(1, articleRepository.count())
        assertFalse(articleRepository.findById(article.id!!).isPresent)
    }

    @Test
    fun `given Author get all articles`() {
        val articles = articleRepository.findAllByAuthorsIn(listOf(author))
        assertEquals(1, articles.count())
        assertEquals(article2, articles.iterator().next())
    }

    @Test
    fun `given Keyword get all articles`() {
        val articles = articleRepository.findAllByKeywordsIn(listOf(keyword))
        assertEquals(1, articles.count())
        assertEquals(article2, articles.iterator().next())
    }

    @Test
    fun `given Period get all articles`() {
        val articles = articleRepository.findAllByPublishDateBetween(
            LocalDateTime.parse("2019-10-01T12:00:00"), LocalDateTime.parse("3000-10-21T12:00:00"))
        assertEquals(2, articles.count())
        (articles.iterator().forEach { a -> assertTrue(setOf(article, article2).contains(a)) })
    }
}
