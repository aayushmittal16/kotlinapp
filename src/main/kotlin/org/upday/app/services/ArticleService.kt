package org.upday.app.services

import java.time.LocalDateTime
import java.util.Optional
import org.springframework.stereotype.Service
import org.upday.app.entities.Article
import org.upday.app.persistence.ArticleRepository
import org.upday.app.persistence.AuthorRepository
import org.upday.app.persistence.KeywordRepository

@Service
class ArticleService(
    private val articleRepository: ArticleRepository,
    private val authorRepository: AuthorRepository,
    private val keywordRepository: KeywordRepository
) {

    fun findById(id: Long): Optional<Article> {
        return articleRepository.findById(id)
    }

    fun save(article: Article): Article {
        return articleRepository.save(article)
    }

    fun deleteById(id: Long) {
        articleRepository.deleteById(id)
    }

    fun findAllByAuthor(name: String): Iterable<Article> {
        return articleRepository.findAllByAuthorsIn(authorRepository.findByName(name))
    }

    fun findAllByKeyword(keyword: String): Iterable<Article> {
        return articleRepository.findAllByKeywordsIn(keywordRepository.findByKeyword(keyword))
    }

    fun findAllByPeriod(from: LocalDateTime, to: LocalDateTime): Iterable<Article> {
        return articleRepository.findAllByPublishDateBetween(from, to)
    }
}
