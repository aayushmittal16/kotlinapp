package org.upday.app.persistence

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.upday.app.entities.Keyword

@Repository
interface KeywordRepository : CrudRepository<Keyword, Long> {
    fun findByKeyword(keyword: String): Iterable<Keyword>
}
