package org.upday.app.persistence

import java.time.LocalDateTime
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.upday.app.entities.Article
import org.upday.app.entities.Author
import org.upday.app.entities.Keyword

@Repository
interface ArticleRepository : CrudRepository<Article, Long> {
    fun findAllByAuthorsIn(authors: Iterable<Author>): Iterable<Article>
    fun findAllByKeywordsIn(keywords: Iterable<Keyword>): Iterable<Article>
    fun findAllByPublishDateBetween(from: LocalDateTime, to: LocalDateTime): Iterable<Article>
}
