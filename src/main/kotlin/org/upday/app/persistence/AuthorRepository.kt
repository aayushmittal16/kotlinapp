package org.upday.app.persistence

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.upday.app.entities.Author

@Repository
interface AuthorRepository : CrudRepository<Author, Long> {
    fun findByName(name: String): Iterable<Author>
}
