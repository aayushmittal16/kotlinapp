package org.upday.app.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import java.net.URI
import java.time.LocalDateTime
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.upday.app.entities.Article
import org.upday.app.services.ArticleService

@RestController
class ArticleController(private val articleService: ArticleService) {

    companion object {
        const val ARTICLE_BASE_URL = "/api/v1/article/"
        const val ARTICLE_BASE_URL_WITH_ID = "/api/v1/article/{id}"
        const val ARTICLES_AUTHOR_URL = "/api/v1/articles/author/{name}"
        const val ARTICLES_KEYWORD_URL = "/api/v1/articles/keyword/{keyword}"
        const val ARTICLES_PERIOD_URL = "/api/v1/articles/from/{from}/to/{to}"
    }

    private val logger = LoggerFactory.getLogger(ArticleController::class.java)

    @Autowired
    lateinit var mapper: ObjectMapper

    @GetMapping(ARTICLE_BASE_URL_WITH_ID)
    fun getArticle(@PathVariable id: Long): ResponseEntity<String> {
        logger.info("Request: GET /api/v1/article/{}", id)
        val article = articleService.findById(id)
        if (article.isEmpty) {
            logger.warn(
                "Could not complete request GET /api/v1/article/{}. Article with given id {} not found.",
                id,
                id
            )
            return ResponseEntity.notFound().build<String>()
        }
        val responseBody = mapper.writeValueAsString(article)
        logger.info("Request: GET /api/v1/article/{} finished successfully", id)
        return ResponseEntity.ok(responseBody)
    }

    @PostMapping(ARTICLE_BASE_URL)
    fun postArticle(@RequestBody article: Article): ResponseEntity<String> {
        logger.info("Request: POST /api/v1/article/", article)
        articleService.save(article)
        val responseBody = mapper.writeValueAsString(article)
        logger.info("Request: POST /api/v1/article/ finished successfully for article id: {}", article.id)
        return ResponseEntity.created(URI("/api/v1/article/${article.id}"))
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .body(responseBody)
    }

    @PatchMapping(ARTICLE_BASE_URL_WITH_ID)
    fun updateArticle(@PathVariable id: Long, @RequestBody article: Article): ResponseEntity<Unit> {
        logger.info("Request: PATCH /api/v1/article/{}", id)
        if (articleService.findById(id).isEmpty) {
            logger.info(
                "Could not complete request: PATCH /api/v1/article/{}. Article with given id {} not found",
                id,
                id
            )
            return ResponseEntity.notFound().build<Unit>()
        }
        articleService.save(article)
        logger.info("Request: PATCH /api/v1/article/{} finished successfully", id)
        return ResponseEntity.noContent().build<Unit>()
    }

    @DeleteMapping(ARTICLE_BASE_URL_WITH_ID)
    fun deleteArticle(@PathVariable id: Long): ResponseEntity<Unit> {
        logger.info("Request: DELETE /api/v1/article/{}", id)
        if (articleService.findById(id).isEmpty) {
            logger.info(
                "Could not complete request: DELETE /api/v1/article/{}. Article with given id {} not found",
                id,
                id
            )
            return ResponseEntity.notFound().build<Unit>()
        }
        articleService.deleteById(id)
        logger.info("Request: DELETE /api/v1/article/{} finished successfully", id)
        return ResponseEntity.noContent().build<Unit>()
    }

    @GetMapping(ARTICLES_AUTHOR_URL)
    fun findArticlesByAuthor(@PathVariable name: String): ResponseEntity<List<Article>> {
        logger.info("Request: GET /api/v1/articles/author/{}", name)
        val resultSet = articleService.findAllByAuthor(name)
        logger.info("Request: GET /api/v1/articles/author/{} finished successfully", name)
        return ResponseEntity.ok(resultSet.toList())
    }

    @GetMapping(ARTICLES_KEYWORD_URL)
    fun findArticlesByKeyword(@PathVariable keyword: String): ResponseEntity<List<Article>> {
        logger.info("Request: GET /api/v1/articles/keyword/{}", keyword)
        val resultSet = articleService.findAllByKeyword(keyword)
        logger.info("Request: GET /api/v1/articles/keyword/{} finished successfully", keyword)
        return ResponseEntity.ok(resultSet.toList())
    }

    @GetMapping(ARTICLES_PERIOD_URL)
    fun findArticlesByPeriod(
        @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) from: LocalDateTime,
        @PathVariable @DateTimeFormat(
                    iso = DateTimeFormat.ISO.DATE_TIME
                ) to: LocalDateTime
    ): ResponseEntity<List<Article>> {
        logger.info("Request: GET /api/v1/articles/from/{}/to/{}", from, to)
        val resultSet = articleService.findAllByPeriod(from, to)
        logger.info("Request: GET /api/v1/articles/from/{}/to/{} finished successfully", from.toString(), to)
        return ResponseEntity.ok(resultSet.toList())
    }
}
