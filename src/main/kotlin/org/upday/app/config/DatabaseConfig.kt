package org.upday.app.config

import java.time.LocalDateTime
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.upday.app.entities.Article
import org.upday.app.entities.Author
import org.upday.app.entities.Keyword
import org.upday.app.persistence.ArticleRepository

@Configuration
class DatabaseConfig {

    @Bean
    fun databaseInitializer(articleRepository: ArticleRepository) = ApplicationRunner {
        articleRepository.save(Article(
            "My first app in Kotlin",
            "How I created my first app in Kotlin with Spring Boot",
            "This is my first app using Kotlin and Spring Boot. " +
                    "This is a test for an engineering position at upday. " + "" +
                    "It involves a tiny bit of coding and mostly magic using frameworks.",
            LocalDateTime.now(),
            mutableSetOf(Author("Aayush Mittal")),
            mutableSetOf(Keyword("Kotlin"), Keyword("Spring Boot"), Keyword("Programming"), Keyword("Engineering"))
        ))
        articleRepository.save(Article(
            "Radiohead on In Rainbows",
            "Thom Yorke and Ed O'Brien joined John Kennedy for an extensive " +
                    "talk through their brilliant new album.",
            "'In Rainbows' is one of their finest albums to date with the band perfectly " +
                    "balancing the mixture of electronics and rock anthems in a hugely enjoyable " +
                    "45 minutes.",
            LocalDateTime.now(),
            mutableSetOf(Author("Thom Yorke"), Author("Ed O'Brien"), Author("John Kennedy")),
            mutableSetOf(Keyword("Radiohead"), Keyword("In Rainbows"), Keyword("music"), Keyword("Best album of 2007"))
        ))
    }
}
