package org.upday.app.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.LocalDateTime
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.persistence.Table
import org.springframework.format.annotation.DateTimeFormat

@Entity
@Table(name = "article")
class Article(
    var title: String,
    var description: String,
    var content: String,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    var publishDate: LocalDateTime = LocalDateTime.now(),

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JsonIgnoreProperties("articles")
    var authors: Set<Author>?,

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JsonIgnoreProperties("articles")
    var keywords: Set<Keyword>?,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null
)
