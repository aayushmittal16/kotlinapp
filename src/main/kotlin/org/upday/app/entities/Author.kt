package org.upday.app.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.persistence.Table

@Entity
@Table(name = "author")
class Author(
    var name: String,

    @ManyToMany(cascade = [CascadeType.ALL], mappedBy = "authors")
    @JsonIgnoreProperties("authors")
    var articles: Set<Article> = mutableSetOf(),

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
)
