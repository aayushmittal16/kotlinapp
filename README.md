# Kotlin app

A challenge to create a REST API that allows CRUD operations for news articles. Also, my first ever program/application written in Kotlin.

This application has already been initialized with two articles using `DatabaseConfig` so you can already play around by calling the REST APIs.

## Tech Stack
* Spring Boot
* H2 database
* Kotlin
* Swagger
* Docker
* Maven
* ktlint
* SLF4J

## Quick Usage
Navigate to project directory and run the following command:

`./run.sh`

To open Swagger UI to see all available endpoints:

`/swagger-ui.html`

## Endpoints

```
GET /health

GET /api/v1/article/{id}

POST /api/v1/article

PATCH /api/v1/article/{id}

DELETE /api/v1/article/{id}

GET /api/v1/articles/author/{name}

GET /api/v1/articles/keyword/{keyword}

GET /api/v1/articles/from/{from}/to/{to}
```
## Explanation
The above script runs `Docker` commands:

`docker build -t app .`

`docker run -p 8080:8080 -t app`

Docker build fetches `Maven` image, copies source to `/home/app/` and runs `mvn clean install`. This command cleans the `target` directory and downloads all maven repositories required for this project. Then it runs `ktlint` to check for formatting and possible errors. Then it runs all tests available in the project. Eventually, it creates a JAR file inside `target` directory.

Docker build then fetches `openjdk 11` image from Docker registry, exposes port `8080` and executes the JAR file which will continue running unless killed manually.

## Severe Limitation
For finding articles by period, the only date format that works is `2019-10-21T12:00:00`. Obviously this is not practical enough and needs to be customized to account for more formats especially one with just dates and no time component.

### If I had more time, I'd:
* configure Swagger to only show response codes that are possible based on current logic. For e.g., `/api/v1/article/` shows response codes: `201, 401, 403, 404` but only `201 and 409` are currently supported in the application. So this is an incomplete feature.
* support pagination using offset and limit parameters because it wouldn't be a good idea to return 1000s of articles at once.
* introduce DTOs instead of using JPA entities directly on view layer because DTOs provide more than just a resource. They can wrap around entities and additionally contain hyperlinks for ease of navigation by tracking previous page, next page, total objects available, etc.
* add unit tests for services and repositories as the application starts to get a bit more complex. Same goes with logging activity on these layers.
* fix the limitation with handling dates.

## Writing part of the assignment:

* My proudest achievement is developing a batch processing service to ingest big data for music licensing based on music streaming activities on services such as Spotify. I created a publisher/subscriber model based service that would run asynchronously and highly concurrent in nature, thus, providing very high scalability. I initially deployed it on hardware on premise and I was able to run more than 60 jobs in parallel leveraging full hardware capability. I saw a lot of cases of bottleneck that I had to eliminate to make it scalable even further. I became aware recently from my previous colleagues that it is now deployed on AWS and it is helping the company take on new customers relatively easier. The company had $0 in revenues from licensing online streaming activity in 2016 and they recently hit $1billion mark in 3 years. This service definitely played an important role in it.
* I prefer reading articles over books. I like blogs on `Medium` from individual users as well as companies. [Netflix Technology Blog](https://medium.com/@NetflixTechBlog) and [Baeldung](https://www.baeldung.com/) are the ones that I enjoy most. I definitely have one favorite blog/repository that I'd highly recommend to anyone who wants to understand microservices architecture and a quick but thorough implementation. It is a repository on Github: [Piggy Metrics](https://github.com/sqshq/PiggyMetrics).
* Unfortunately I do not have access to the app because I do not own any Samsung devices.